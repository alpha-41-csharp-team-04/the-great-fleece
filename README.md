# The Great Fleece
The Great Fleece is created following the course ["The Ultiamate Guide to Cinematography with Unity"](https://risesmart.udemy.com/course/the-ultimate-guide-to-game-development-with-unity/learn/lecture/34331704#overview) by Jonathan Weinberger.
It introduces cutscene development uning timeline and cinemachine and teaches more intermediate C# game programming concepts. It also explores lighting and post-processing effects and uses an asset pack downloaded from the [assetstore] and created specially for the course.
The course itself is constructed on learn by doing principle; each game feature is introduced as a challenge and while the game design and the assets used are fully copied from  the course, the huge part of the C# implementation is my own work and in many places diviates significantly from the code provided by the course solution. 

## Project status
- 18-Dec-2022: Create the Gitlab repository and the assetstore main assets.
